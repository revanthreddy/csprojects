package com.github.revreddy;

/*
//
// Project 20 - Music com.github.revreddy.Database
//     Create a database for storing a customer-download-song schema,
//      parse data from file into the db, and perform a series of query
//      operations on the db.
//
// Created by Revanth Reddy 3/16/2015
// Language: Java, SQL
// Environ: Mac OSX 10.10, IntelliJ IDEA 14.0.3, Java 8
//          MySQL Community Server 5.1.73, MySQL Connector/J 5.1.34
//
// Copyright (c) 2015 Revanth Reddy. All rights reserved.
//
 */

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.StringTokenizer;


public class Database
{
    // JDBC driver name and database URL
    static final String jdbc_driver = "com.mysql.jdbc.Driver";
    static final String db_url = "jdbc:mysql://localhost/";

    // Database credentials
    static final String userName = "root";
    static final String password = "";

    // Filepaths (change to point to correct directories )
    static final String path_customer = "customer.txt";
    static final String path_download = "download.txt";
    static final String path_song = "song.txt";

    //Connection, statement fields
    private static Connection conn;
    private static Statement stmt;

    public Database () throws SQLException {
        conn = openConnection();
        stmt = conn.createStatement();
    }

    public static Connection openConnection() {
        Connection conn = null;

        try
        {
            Class.forName (jdbc_driver).newInstance ();
            conn = DriverManager.getConnection(db_url, userName, password);
            System.out.println ("Connection established");
        }
        catch (Exception e)
        {
            System.err.println ("Cannot connect to database server");
        }

        return conn;
    }

    public static void closeConnection() {
        if (conn != null) {
            try {
                stmt.close();
                conn.close();
                System.out.println("Connection terminated");
            } catch (Exception e) { /* ignore close errors */ }
        }
    }

    public void initializeDB() throws SQLException {
        String sql;

        // Delete db if already exists
        sql = "DROP DATABASE IF EXISTS music";
        this.stmt.executeUpdate(sql);

        // Create db
        System.out.println("Create database");
        sql = "CREATE DATABASE music";
        this.stmt.executeUpdate(sql);

        // Use db
        System.out.println("Access database");
        sql = "USE music";
        this.stmt.executeUpdate(sql);
    }

    public void deleteDB() throws SQLException {
        // Delete db
        String sql = "DROP DATABASE music";
        this.stmt.executeUpdate(sql);
        System.out.println("Database deleted.");
    }

    public void createTables() throws SQLException {
        System.out.println("Creating tables");
        String sql;
        sql = "CREATE TABLE customer " +
                "(ssn INT not NULL, " +
                " cname VARCHAR(20), " +
                " gender VARCHAR(20), " +
                " age INT, " +
                " profession VARCHAR(20), " +
                "PRIMARY KEY (ssn))";
        stmt.executeUpdate(sql);

        sql = "CREATE TABLE download " +
                "(ssn INT not NULL, " +
                " sid INT not NULL, " +
                " time INT, " +
                "PRIMARY KEY (ssn, sid))";
        stmt.executeUpdate(sql);

        sql = "CREATE TABLE song " +
                "(sid INT not NULL, " +
                " sname VARCHAR(20), " +
                " singer VARCHAR(20), " +
                " price INT, " +
                "PRIMARY KEY (sid))";
        stmt.executeUpdate(sql);
    }

    public void insertData() throws IOException, SQLException {
        this.insertCustomer();
        this.insertDownload();
        this.insertSong();
    }

    public void insertCustomer() throws SQLException, IOException {
        System.out.println("Inserting customer data");
        BufferedReader reader = new BufferedReader(new FileReader(path_customer));

        String line;
        PreparedStatement insert = conn.prepareStatement("INSERT INTO customer VALUES (?, ?, ?, ?, ?)");
        while ((line = reader.readLine()) != null) {
            // Parse the line for each attribute
            StringTokenizer tok = new StringTokenizer(line, ",");
            int ssn = Integer.parseInt(tok.nextToken());
            String cname = tok.nextToken();
            String gender = tok.nextToken();
            int age = Integer.parseInt(tok.nextToken());
            String profession = tok.nextToken();

            // Create the insert query and update db
            insert.setInt(1, ssn);
            insert.setString(2, cname);
            insert.setString(3, gender);
            insert.setInt(4,age);
            insert.setString(5,profession);
            insert.executeUpdate();
        }
    }

    public void insertDownload() throws SQLException, IOException {
        System.out.println("Inserting download data");
        BufferedReader reader = new BufferedReader(new FileReader(path_download));

        String line;
        PreparedStatement insert = conn.prepareStatement("INSERT INTO download VALUES (?, ?, ?)");
        while ((line = reader.readLine()) != null) {
            // Parse the line for each attribute
            StringTokenizer tok = new StringTokenizer(line, ",");
            int ssn = Integer.parseInt(tok.nextToken());
            int sid = Integer.parseInt(tok.nextToken());
            int time = Integer.parseInt(tok.nextToken());

            // Create the insert query and update db
            insert.setInt(1, ssn);
            insert.setInt(2, sid);
            insert.setInt(3, time);
            insert.executeUpdate();
        }
    }

    public void insertSong() throws SQLException, IOException {
        System.out.println("Inserting song data");
        BufferedReader reader = new BufferedReader(new FileReader(path_song));

        String line;
        PreparedStatement parser = conn.prepareStatement("INSERT INTO song VALUES (?, ?, ?, ?)");
        while ((line = reader.readLine()) != null) {
            // Parse the line for each attribute
            StringTokenizer tok = new StringTokenizer(line, ",");
            int sid = Integer.parseInt(tok.nextToken());
            String sname = tok.nextToken();
            String singer = tok.nextToken();
            int price = Integer.parseInt(tok.nextToken());

            // Create the insert query and update db
            parser.setInt(1, sid);
            parser.setString(2, sname);
            parser.setString(3, singer);
            parser.setInt(4, price);
            parser.executeUpdate();
        }
        parser.close();
    }

    public void displayAllTables() throws SQLException {
        System.out.println();
        this.displayCustomer();
        this.displayDownload();
        this.displaySong();
    }

    public void displayCustomer() throws SQLException {
        System.out.println("Display table: customer");
        String sql = "SELECT * FROM customer ORDER BY ssn ASC ";
        ResultSet rs = stmt.executeQuery(sql);
        System.out.println("---------------------------------------------");
        System.out.println("ssn         cname     gender  age profession");
        System.out.println("---------------------------------------------");
        while(rs.next()){
            //Retrieve by column name
            int ssn  = rs.getInt("ssn");
            String cname = rs.getString("cname");
            String gender = rs.getString("gender");
            int age  = rs.getInt("age");
            String profession = rs.getString("profession");

            //Display values
            System.out.print(String.format("%-10s" , ssn));
            System.out.print(String.format("%-12s" , cname));
            System.out.print(String.format("%-8s" , gender));
            System.out.print(String.format("%-4s" , age));
            System.out.println(profession);
        }
        System.out.println();
        rs.close();
    }

    public void displayDownload() throws SQLException {
        System.out.println("Display table: download");
        String sql = "SELECT * FROM download ORDER BY ssn ASC ";
        ResultSet rs = stmt.executeQuery(sql);
        System.out.println("------------------------");
        System.out.println("ssn       sid   time");
        System.out.println("------------------------");
        while(rs.next()){
            //Retrieve by column name
            int ssn  = rs.getInt("ssn");
            int sid  = rs.getInt("sid");
            int time  = rs.getInt("time");

            //Display values
            System.out.print(String.format("%-10s" , ssn));
            System.out.print(String.format("%-6s" , sid));
            System.out.println(time);
        }
        System.out.println();
        rs.close();
    }

    public void displaySong() throws SQLException {
        System.out.println("Display table: song");
        String sql = "SELECT * FROM song ORDER BY sid ASC ";
        ResultSet rs = stmt.executeQuery(sql);
        System.out.println("-------------------------------------------");
        System.out.println("sid   sname     singer        price");
        System.out.println("-------------------------------------------");
        while(rs.next()){
            //Retrieve by column name
            int sid  = rs.getInt("sid");
            String sname = rs.getString("sname");
            String singer = rs.getString("singer");
            int price  = rs.getInt("price");

            //Display values
            System.out.print(String.format("%-6s" , sid));
            System.out.print(String.format("%-10s" , sname));
            System.out.print(String.format("%-14s" , singer));
            System.out.println(price);
        }
        System.out.println();
        rs.close();
    }

    public static void main (String[] args) throws SQLException, IOException {
        // Open connection
        Database db = new Database();
        String sql;

        // Create/Open db
        db.initializeDB();

        // Create tables
        db.createTables();

        // Insert data into tables
        db.insertData();

        // Display tables
        db.displayAllTables();

        // Cleanup
        db.deleteDB();
        db.closeConnection();
        System.out.println("Done");

    }
}