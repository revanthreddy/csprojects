### CSPROJECTS

---
#####Description:
Projects in this repository were created as part of various course work using the various guidelines and restrictions outlined for that particular project. The approaches used may not be considered best or efficient real-world implementation as programming assignments often attempt to force programming with rudimentary syntax with limited library access. This helps emphasize the learning aspects of those concepts and abstractions.

---
#####Version number format:
v[project#].[course#].[major_update#].[minor_update#]
